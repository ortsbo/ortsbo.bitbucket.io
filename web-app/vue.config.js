module.exports = {
    configureWebpack: {
        output: {
            filename: '[name].js',
            chunkFilename: '[id][chunkhash].js'
        },
        optimization: {
            splitChunks: {
                cacheGroups: {
                    node_vendors: {
                        test: /[\\/]node_modules[\\/]/,
                        chunks: "all",
                        priority: 1
                    }
                }
            }
        },
    },
    filenameHashing: false,
    css: {
        extract: true
    }
}